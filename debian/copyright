Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Random
Upstream-Contact: Geoffrey Rommel <grommel@cpan.org>
Source: https://metacpan.org/release/Math-Random
Disclaimer: This package is not part of the Debian operating system.
 The libmath-random-perl source and binary Debian packages are part of the
 "non-free" area in our archive. The packages in this area are not part of the
 Debian system, although they have been configured for use with Debian. We
 nevertheless support the use of the libmath-random-perl source and binary
 Debian packages and provide infrastructure for non-free packages (such as our
 bug tracking system and mailing lists).

Files: *
Copyright: 1997, John Venier <jvenier@mdanderson.org>
 1997, Barry W. Brown <bwb@mdaali.cancer.utexas.edu>
 1997, Geoffrey Rommel <grommel@cpan.org>
License: Artistic or GPL-1+

Files: randlib.c com.c randlib.h
Copyright: Association for Computing Machinery
License: ACM
  Submittal of an algorithm for publication in one of the ACM Transactions
  implies that unrestricted use of the algorithm within a computer is
  permissible. General permission to copy and distribute the algorithm
  without fee is granted provided that the copies are not made or
  distributed for direct commercial advantage. The ACM copyright notice
  and the title of the publication and its date appear, and notice is
  given that copying is by permission of the Association for Computing
  Machinery. To copy otherwise, or to republish, requires a fee and/or
  specific permission.
 .
  Krogh, F.  "Algorithms Policy."  ACM  Tran.  Math.  Softw.  13
  (1987), 183-186.
 .
 Note, however, that only the particular expression of an algorithm
 can be copyrighted, not the algorithm per se; see 17 USC 102(b).
 .
 The authors of Math::Random placed the Randlib code that we have written in
 the public domain.

Files: debian/*
Copyright: 2008-2009, Charles Plessy <plessy@debian.org>
 2009, Jonathan Yu <jawnsy@cpan.org>
 2009, Ryan Niebur <ryan@debian.org>
 2014-2021, gregor herrmann <gregoa@debian.org>
License: Poetic
 This work ‘as-is’ we provide.
 No warranty, express or implied.
 We’ve done our best,
 to debug and test.
 Liability for damages denied.
 .
 Permission is granted hereby,
 to copy, share, and modify.
 Use as is fit,
 free or for profit.
 On this notice these rights rely.
Comment: The text of this license is © 2005 Alexander E Genaud,
 see http://genaud.net/2005/10/poetic-license/.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
